# -*- coding: utf-8 -*-
"""
Created on Fri Dec 01 09:54:05 2017

@author: joshuac
"""
import csv
import datetime
from pytz import timezone

importColumn = 3
serialColumn = 0
dateColumn = 7
timeColumn = 6
class importer:
    def importPrepare(self, importFile):
        return 0
    
    def importData(self, importFile):
        pointsList = {}
        with open(importFile) as csvfile:
            reader = csv.reader(csvfile, delimiter=';', quotechar='|')
            for row in reader:
                if(row[0] == ""): 
                    continue
                #Add rows to dictionary in here
                fcuName = row[serialColumn].replace("\"","")
                pointsList[fcuName] = {}
                date = row[dateColumn]
                time = row[timeColumn]
                startTime = datetime.datetime.strptime(date + " " + time, "%d/%m/%Y %I:%M:%S %p").replace(tzinfo=timezone('Etc/GMT-10'))
                pointsList[fcuName][startTime] = float(row[importColumn])*1000
        return pointsList