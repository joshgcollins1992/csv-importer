# -*- coding: utf-8 -*-

import sys
import imp
import logging
import json
import pyhaystack
import math
from pyhaystack.client.widesky import WideskyHaystackSession
from pyhaystack.client import get_instance
import traceback

global dataImporter
WideSkyClientSettings = {
        'implementation': 'widesky.WideskyHaystackSession'
        }

# Logging configuration
logFormatter = logging.Formatter("%(asctime)s [%(levelname)-5.5s]  %(message)s")
rootLogger = logging.getLogger()
logging.getLogger("pyhaystack").setLevel(logging.CRITICAL)



rootLogger.setLevel(logging.INFO)
consoleHandler = logging.StreamHandler(sys.stdout)
consoleFormatter = logging.Formatter("[%(levelname)-5.5s] %(message)s")
consoleHandler.setFormatter(consoleFormatter)
rootLogger.addHandler(consoleHandler)

dryRun = False
verbose = False

def main():
    global dryRun
    global verbose
    importerProgram = ""
    importFile = ""
    jsonConfig = ""
    if len(sys.argv) < 2 or sys.argv[1] == "--help":
        printHelpInfo()
        sys.exit(1)
    else:
        for i in range(1, len(sys.argv)):
            if(sys.argv[i]=="--importerProgram"):
                importerProgram = sys.argv[i+1]
            elif(sys.argv[i]=="--importFile"):
                importFile = sys.argv[i+1]
            elif(sys.argv[i]=="--jsonMapping"):
                jsonConfig = sys.argv[i+1]
            elif(sys.argv[i]=="--dryRun"):
                dryRun = True
            elif(sys.argv[i]=="-v"):
                verbose = True
            elif(sys.argv[i] == "--uri"):
                WideSkyClientSettings["uri"] = sys.argv[i+1]
            elif(sys.argv[i] == "--client_id"):
                WideSkyClientSettings["client_id"] = sys.argv[i+1]
            elif(sys.argv[i] == "--client_secret"):
                WideSkyClientSettings["client_secret"] = sys.argv[i+1]
            elif(sys.argv[i] == "--username"):
                WideSkyClientSettings["username"] = sys.argv[i+1]
            elif(sys.argv[i] == "--password"):
                WideSkyClientSettings["password"] = sys.argv[i+1]
                print(WideSkyClientSettings["password"])
    #Check all required parameters supplied
    if( (not dryRun and ("username" not in WideSkyClientSettings or \
       "password" not in WideSkyClientSettings or \
       "uri" not in WideSkyClientSettings or \
       "client_id" not in WideSkyClientSettings or \
       "client_secret" not in WideSkyClientSettings)) or \
       importerProgram == "" or \
       importFile == "" or \
       jsonConfig ==""):
        printHelpInfo()
        sys.exit(1)
        
    #Setup logging
    fileHandler = logging.FileHandler("{0}/{1}.log".format("./", "csv-importer"))
    fileHandler.setFormatter(logFormatter)
    rootLogger.addHandler(fileHandler)
    
    #Load importer program
    importer = imp.load_source("importers." + importerProgram,importerProgram)
    client = None
    if not dryRun: 
    	connectToWideSky()
    	client_settings = build_ws_client_settings()
    	client = get_instance(**client_settings)
    #Declare importer class
    dataImporter = importer.importer()
    importPrepare(dataImporter, importFile)
    pointsList = importData(dataImporter, importFile)
    pointsMapping = mappingsFromConfig(jsonConfig)
    print(pointsMapping)
    wideSkyPush(client, pointsList, pointsMapping)
    logging.info("Data upload complete!")
    
def importPrepare(importer, importFile):
    #Call importer preparation function
    logging.info("Preparing for import...")
    return importer.importPrepare(importFile)

def importData(importer, importFile):
    #Call importer csv data extraction function
    logging.info("Importing data from csv...")
    return importer.importData(importFile)

def mappingsFromConfig(jsonConfig):
    #Generate mappings from site names to WideSky point names
    logging.info("Generating mappings from json file...")
    with open(jsonConfig, 'r') as parameters:
        data=parameters.read().replace('\n', '')
    jsonObject = json.loads(data)
    return jsonObject

def wideSkyPush(client, pointsList, pointsMapping):
    #Push all points extracted from csv into WideSky
    logging.info("Pushing data to WideSky...")
    sliceLength = 1000
    for key in pointsList:
        if(key in pointsList and key in pointsMapping):
            logging.info("Uploading from " + key + " to " + pointsMapping[key])
            try:
                if verbose:
                    print(pointsMapping[key])
                slices = math.ceil(len(pointsList[key])/sliceLength)
                logging.info("Writing " + str(slices) + " slices")
                i = 0
                timeList = sorted(pointsList[key].keys())
                while i <= slices:
                    timeSlice = timeList[i*sliceLength:(i+1)*sliceLength]
                    if(len(timeSlice) == 0):
                        i = i + 1
                        continue
                    elif(len(timeSlice) == 1):
                        logging.info("Writing time " + str(timeSlice[0]))
                    else:
                        logging.info("Writing slice " + str(timeSlice[0]) + " to " + str(timeSlice[-1]))
                    sliceDict = {k: pointsList[key][k] for k in timeSlice}
                    if verbose:
                        print(pointsMapping[key])
                        if("importWh" in pointsMapping[key]):
                            for element in sorted(sliceDict.keys()):
                                print("\t" + str(element) + ": " + str(sliceDict[element]))
                    if not dryRun:
                        status  = client.his_write(pointsMapping[key], sliceDict)
                        status.wait()
                        if(status.is_failed):
                            logging.warning("Could not upload data point '" + pointsMapping[key] + "'")
                            status.result
                    i = i+1
            except (pyhaystack.client.http.exceptions.HTTPStatusError) as ex:
                print(ex)
                logging.warning("Could not upload data point '" + pointsMapping[key] + "'")
                pass
            
        else:
            logging.warning("'" + key + "' not found in mapping json")
                
def printHelpInfo():
    print("Usage: python csv-importer.py --importerProgram filename",\
                "--importFile filename --jsonMapping filename",\
                "--uri WideSky URI --client_id WideSkyClientID",\
                "--client_secret WideSkyClientSecret",\
                "--username WideSkyUsername --password WideSkyPassword [--dryRun] [-v]")

def connectToWideSky():
    session = WideskyHaystackSession(uri=WideSkyClientSettings["uri"], \
                                                username=WideSkyClientSettings["username"], \
                                                password=WideSkyClientSettings["password"], \
                                                client_id=WideSkyClientSettings["client_id"], \
                                                client_secret=WideSkyClientSettings["client_secret"], \
                                                pint=True)
    #print("before testconnection")
    testConnection = session.about()
    #print(testConnection)
    try:
        testConnection.result
        #logging.info("I should be testing the connection here")
    except Exception as e:
        logging.error(repr(e))
        logging.error(traceback.print_stack())
        logging.error("WideSky connection failed")
        sys.exit(1)
    logging.info("Connected to WideSky!")     
    return session

def build_ws_client_settings():
    return WideSkyClientSettings

if __name__ == '__main__':
    main()
