# csv-importer

Main program used for extracting from files and pushing to WideSky. Requires a json file to map from device names in the file to WideSky point names, and a custom python program for extracting the points from the file. Examples are in Barangaroo project folder (P15609) for excel import, and Iglu Redfern (P16784) for csv import.



Usage: 
python csv-importer.py --importerProgram <Path to python importer program> --importFile <File for import> --jsonMapping <Json mapping file> --uri <WideSky API URI> --client_id <Client ID> --client_secret <Client Secret> --username <Username> --password <password>