# -*- coding: utf-8 -*-
"""
Created on Fri Dec 01 09:54:05 2017

@author: joshuac
"""
import csv
import datetime
from dateutil.parser import parse
#import ciso8601
from pytz import timezone

timeColumn = 0


class importer:
    def importPrepare(self, importFile):
        return 0
    
    def importData(self, importFile):
        pointsList = {}
        pointNames = []
        with open(importFile) as csvfile:
            reader = csv.reader(csvfile, delimiter=',', quotechar='|')
            i = 1
            for row in reader:
                if(i==1):
                    j = 0
                    for element in row:
                        if(j > 0):
                            pointNames.append(element)
                            print(element)
                            pointsList[element] = {}
                        j = j + 1
                if(i>1):
                    j = 0
                    if(row[timeColumn] == ""):
                        continue
                    #Add rows to dictionary in here
                    #startTime = dateutil.parser.isoparse(row[timeColumn])
                    #print startTime
                    #startTime = datetime.datetime.strptime(row[timeColumn].replace(":", "-"), "%Y-%m-%dT%H-%M-%S-%z")
                    #print startTime
                    #startTime = datetime.datetime.strptime(row[timeColumn], "%d/%m/%Y %H:%M:%S", tzinfo=timezone('Australia/Brisbane'))
                    startTime = datetime.datetime.strptime(row[timeColumn], "%d/%m/%Y %H:%M").replace(tzinfo=timezone('Etc/GMT-10'))
                    for element in row:
                        #print(element)
                        if(j > 0):
                            if(element != "null"):
                                try:
                                    #float(element)
                                    pointsList[pointNames[j-1]][startTime] = float(element)
                                except ValueError:
                                    filler = 1
                                    #print (str(element) + " is not a float (column " + str(j) + ", row " + str(i) + ")")
                            #print startTime
                            # print pointNames[j-1]
                            # print element
                        j = j + 1
                i = i + 1 
        return pointsList