# -*- coding: utf-8 -*-
"""
Created on Fri Dec 01 09:54:05 2017

@author: joshuac
"""
import csv
import datetime
from pytz import timezone

timeColumn = 1
pointMapping = []
#expColumn = 2
#impColumn = 4
#litresColumn = 3

class importer:
    def importPrepare(self, importFile):
        return 0
    
    def importData(self, importFile):
        pointsList = {}
        with open(importFile) as csvfile:
            reader = csv.reader(csvfile, delimiter='\t', quotechar='|')
            i = 0
            serialNumber = importFile.split("/")[-1].split("_")[0]
            for row in reader:
                if(i==4):
                    for j in range(0,len(row)):
                        mappedName = serialNumber + "_" + row[j]
                        pointMapping.append(mappedName)
                if(i>5):
                    if(len(row) > 2):
                        for j in range(2,len(row)):
                            #Add rows to dictionary in here
                            startTime = datetime.datetime.strptime(row[timeColumn], "%d/%m/%Y %H:%M:%S").replace(tzinfo=timezone('Etc/GMT-10'))
                            if(pointMapping[j] not in pointsList):
                                pointsList[pointMapping[j]] = {}
                            try:
                                #print(row[j])
                                pointsList[pointMapping[j]][startTime] = float(row[j])
                            except ValueError:
                                pass
                                #print("Value is not a float")
                i = i + 1 
        return pointsList