# -*- coding: utf-8 -*-
"""
Created on Fri Dec 01 09:54:05 2017

@author: joshuac
"""
import csv
import datetime
from pytz import timezone

timeColumn = 1
expColumn = 2
impColumn = 4
litresColumn = 3


class importer:
    def importPrepare(self, importFile):
        return 0
    
    def importData(self, importFile):
        pointsList = {}
        with open(importFile) as csvfile:
            reader = csv.reader(csvfile, delimiter='\t', quotechar='|')
            i = 1
            for row in reader:
                if(i>7):
                    #Add rows to dictionary in here
                    #print(row)
                    startTime = datetime.datetime.strptime(row[timeColumn], "%d/%m/%Y %H:%M:%S").replace(tzinfo=timezone('Etc/GMT-10'))
                    if("exp" not in pointsList):
                        pointsList['exp'] = {}
                    pointsList['exp'][startTime] = float(row[expColumn].strip())
                    if("imp" not in pointsList):
                        pointsList['imp'] = {}
                    pointsList['imp'][startTime] = float(row[impColumn].strip())
                    if("litres" not in pointsList):
                        pointsList['litres'] = {}
                    pointsList['litres'][startTime] = float(row[litresColumn].strip())
                i = i + 1 
        return pointsList