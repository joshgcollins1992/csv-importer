# -*- coding: utf-8 -*-
"""
Created on Fri Dec 01 09:54:05 2017

@author: joshuac
"""
import csv
import datetime
from pytz import timezone

timeColumn = 1
pointMapping = []

class importer:
    def importPrepare(self, importFile):
        return 0
    
    def importData(self, importFile):
        pointsList = {}
        with open(importFile) as csvfile:
            reader = csv.reader(csvfile, delimiter=',', quotechar='|')
            i = 0
            serialNumber = importFile.split("/")[-1].split("_")[0]
            for row in reader:
                if(i==0):
                    for j in range(0,len(row)):
                        mappedName = serialNumber + "_" + row[j]
                        pointMapping.append(mappedName)
                if(i>1):
                    if(len(row) > 2):
                        #Add rows to dictionary in here
                        startTime = datetime.datetime.strptime(row[timeColumn], "%Y-%m-%d %H:%M:%S.%f").replace(tzinfo=timezone('Etc/GMT-10')) + datetime.timedelta(hours=2)
                        
                        for j in range(2,len(row)):
                            if(pointMapping[j] not in pointsList):
                                pointsList[pointMapping[j]] = {}
                            if("kWh" in pointMapping[j] or "kvarh" in pointMapping[j]):
                                pointsList[pointMapping[j]][startTime] = int(row[j])*10
                            else:
                                pointsList[pointMapping[j]][startTime] = float(row[j])
                i = i + 1 
        return pointsList
